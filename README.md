![Logo Image](./logo.png)

# Partnerbase Image Generator

An app that looks at all the companies inside Partnerbase database
and generates a PNG image of their network graphs.

## How it works

1. It fetches the Partnerbase's sitemap index
2. It fetches the sitemap pages detailing the company pages
3. It then extracts the companies slugs from the sitemaps
4. It uses the graph data from the Partnerbase API to generate the network graph
5. For each company, it spawns a puppeteer page and takes a screenshot of the network graph
6. It saves the screenshots in the `images` directory

## How to run

Make sure the Partnerbase backend is up and running. Check the `.env` file for the `SERVER_API_BASE_URL`.
It instructs the app where to find the Partnerbase API.

### Local

To run the app locally, make sure you have node 20 installed.
Then use the following commands:

```bash
yarn install
yarn start
```

Uncomment the line in `.env` file that points to the server running in a container.
```bash
SERVER_API_BASE_URL=http://localhost:38400/ # Running the app locally; server runs in container
```

### Docker

This app architecture follows the other Crossbeam apps.
To have it running in a Docker container
follow the same steps as with the other projects:

```bash
./bin/up
./bin/logs
```

This will build the Docker image and start the container.

Uncomment the proper line in `.env` file:
```bash
SERVER_API_BASE_URL=http://partnerbase-api:3000/ # Running the app in a container; server runs in container
```

### Notes

The above commands will install dependencies and run the app.
The code will let you know of the state it's in by printing messages to the console.
If you're running the app in a container, you can check the logs by running `./bin/logs`.
