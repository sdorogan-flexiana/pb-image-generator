# vim: ft=dockerfile
FROM node:22.0-bookworm-slim
ARG UID=0
ARG USER=root
ARG SERVER_API_BASE_URL
ENV NODE_PATH node_modules
ENV PATH node_modules/.bin:$PATH
ENV PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium

RUN apt-get update && \
 apt-get install -yq gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 \
 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 \
 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 \
 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 \
 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget git bash curl gnupg

RUN apt-get update \
    && apt-get update \
    && apt-get install -y chromium fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf libxss1 \
      --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /app && \
    if [ $UID != 0 ]; then \
        ( getent passwd $UID && \
          usermod --login $USER --move-home --home /home/$USER node || \
          adduser --disabled-password -u $UID $USER \
        ) && \
        chown $USER /app; \
    fi

COPY --chown=$USER:$USER . /app

USER $USER
WORKDIR /app

RUN yarn install --frozen-lockfile

CMD ["yarn", "start"]
