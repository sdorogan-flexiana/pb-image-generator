async function getLogoUrl (node) {
    const logoUrl = node.logoUrl || `https://logo.clearbit.com/${node.domains[0]}?size=96`
    const fallbackUrl = 'https://www.partnerbase.com/svg/crossbeam-company-record.svg'

    try {
        const response = await fetch(logoUrl)
        if (!response.ok) {
            throw new Error('Invalid response')
        }
        return logoUrl
    } catch (error) {
        return fallbackUrl
    }
}

async function rawDataToCytoscape (rawData) {
    const nodePromises = rawData.nodes.map(async (n) => ({
      data: {
        id: n.id,
        domain: n.domains[0],
        logo: await getLogoUrl(n)
      }
    }))
  return {
    nodes: await Promise.all(nodePromises),
    edges: rawData.links.map((e) => ({
      data: {
        id: `${e.companyAId}-${e.companyBId}`,
        source: e.companyAId,
        target: e.companyBId,
        label: e.type
      }
    }))
  }
}

const cytoscapeOptions = {
  layout: {
    name: 'concentric',
    animate: false
  },
  userZoomingEnabled: false,
  style: [
    {
      selector: 'node',
      style: {
        'background-image': 'data(logo)',
        'background-fit': 'contain',
        'background-color': '#fff',
      }
    },
    {
      selector: 'edge',
      style: {
        width: 2
      }
    },
    {
      selector: 'edge[label="Technology Partner"]',
      style: {
        'line-color': '#ed1e79'
      }
    },
    {
      selector: 'edge[label="Channel Partner"]',
      style: {
        'line-color': '#0be8e6'
      }
    }
  ]
}

/*
 * Split an array into chunks of a specified size
 * @param {Array} array - The array to split
 * @param {number} chunkSize - The size of each chunk
 * @returns {Array} - An array of arrays, each containing a chunk of the original array
 */
function chunkArray(array, chunkSize) {
    const chunks = [];
    for (let i = 0; i < array.length; i += chunkSize) {
        chunks.push(array.slice(i, i + chunkSize));
    }
    return chunks;
}

module.exports = { rawDataToCytoscape, cytoscapeOptions, chunkArray }
