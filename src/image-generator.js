const { mkdir, writeFile } = require('node:fs/promises')
const { join } = require('node:path')
const cytosnap = require('cytosnap')
const { XMLParser } = require('fast-xml-parser')
const { rawDataToCytoscape, cytoscapeOptions, chunkArray } = require('./utils')

class ImageGenerator {

    // @type {Cytosnap}
    snap
    // @type {XMLParser}
    xmlParser
    // @type {string[]}
    companiesSiteMaps
    // @type {string}
    imagesFolder
    // @param {string}
    pbUrl
    // @type {number}
    // @default 50
    maxNrOfTabs

    /*
     * Create a new ImageGenerator
     * @param {string} pbUrl - The base URL of the PartnerBase API
     * @param {number} maxNrOfTabs - The maximum number of puppeteer tabs to open at once. Default is 50.
     */
    constructor(pbUrl, maxNrOfTabs = 50) {
        this.snap = cytosnap()
        this.xmlParser = new XMLParser()
        this.pbUrl = pbUrl
        this.maxNrOfTabs = maxNrOfTabs
    }

    async setup() {
        console.log('Setting up image generator...')

        const getSitemaps = async () => {
            const sitemap = await fetch(
                this.pbUrl + 'www-sitemaps/sitemap-index.xml'
            )
            const sitemapText = await sitemap.text()
            console.log('Loaded sitemap index.')

            const sitemapJson = this.xmlParser.parse(sitemapText)
            const companiesSiteMaps = sitemapJson.sitemapindex.sitemap
            .map(sitemap => sitemap.loc)
            .filter(loc => loc.includes('companies'))

            return companiesSiteMaps
        }

        // Make sure images directory exists
        this.imagesFolder = join(__dirname, '..', 'images');
        const mkdirPromise = mkdir(this.imagesFolder, { recursive: true })

        const snapPromise = this.snap.start()
        const companiesSiteMapsPromise = getSitemaps()
        const [,companiesSiteMaps,] = await Promise.all([
            snapPromise,
            companiesSiteMapsPromise,
            mkdirPromise
        ])

        this.companiesSiteMaps = companiesSiteMaps

        console.log(`Setup complete. Found ${companiesSiteMaps.length} company sitemaps.`)
    }

    /*
     * Makes sure a sitemap URL has the same host as the given Partnerbase URL.
     * @param {string} url - The URL to normalize
     * @returns {string} - The normalized URL
     * @throws {Error} - If the URL is invalid
     */
    _normalizeSitemapUrl(url) {
        let result = url
        if (!url.startsWith(this.pbUrl)) {
            const replaceIndex = url.indexOf('www-sitemaps')
            if (replaceIndex === -1) {
                throw new Error('Invalid sitemap URL')
            }
            result = this.pbUrl + url.slice(replaceIndex)
        }
        return result
    }

    /*
     * Get a list of companies from a sitemap
     * @param {string} siteMap - The URL of the sitemap to process
     * @returns {string[]} - The list of company slugs
     */
    async _getCompaniesFromSiteMap(siteMap) {
        siteMap = this._normalizeSitemapUrl(siteMap)
        console.log('Fetching companies from sitemap:', siteMap)

        const companies = await fetch(siteMap)
        if (!companies.ok) {
            throw new Error('Invalid response')
        }
        const companiesText = await companies.text()
        const companiesJson = this.xmlParser.parse(companiesText)

        return companiesJson.urlset.url.map(url => url.loc.slice(url.loc.lastIndexOf('/') + 1))
    }

    /*
     * Generate an image for a company
     * @param {string} slug - The company slug to generate an image for
     * @returns {string} - The image data
     * @throws {Error} - If an error occurs while fetching the graph data or generating the image
     */
    async _generateImage(slug) {
        let graphJson
        try {
            const url = `${this.pbUrl}v0.1/companies/${slug}/graph`
            const graphRequest = await fetch(url)
            if (!graphRequest.ok) {
                throw new Error('Invalid response')
            }
            graphJson = await graphRequest.json()

        } catch (e) {
            console.error('Error fetching graph data for company:', slug, e)
            throw e
        }

        if (graphJson.errors) {
            console.error('Error fetching graph data for company:', slug, graphJson.errors)
            throw new Error('Error fetching graph data')
        }

        const elements = await rawDataToCytoscape(graphJson)
        const options = cytoscapeOptions

        console.log('Generating image for company:', slug)
        try {
            const img = await this.snap.shot({
                elements,
                ...options,
                resolvesTo: 'base64',
                format: 'png',
                width: 640,
                height: 480,
                background: 'transparent'
            })
            return img
        } catch (e) {
            console.error('Error generating image for company:', slug, e)
        }
    }

    /*
     * Save an image for a company
     * @param {string} slug - The company slug to save the image for
     * @param {string} img - The image data to save
     */
    async _saveImage(slug, img) {
        await writeFile(`${this.imagesFolder}/${slug}.png`, img, 'base64')
    }

    /*
     * Generate and save an image for a company
     * @param {string} slug - The company slug to generate an image for
     * @throws {Error} - If an error occurs while generating or saving the image
     */
    async _generateAndSaveImage(slug) {
        let img
        try {
            img = await this._generateImage(slug)
        } catch (e) {
            console.error('Error generating image for company:', slug, 'Skipping.')
            return
        }
        await this._saveImage(slug, img)
    }

    /*
     * Generate images for all companies in a sitemap
     * @param {string} siteMap - The URL of the sitemap to process
     */
    async _generateImagesFromSiteMap(siteMap) {
        const companies = await this._getCompaniesFromSiteMap(siteMap)
        const chunks = chunkArray(companies, this.maxNrOfTabs)

        for (const chunk of chunks) {
            const promises = []
            for (const company of chunk) {
                promises.push(this._generateAndSaveImage(company))
            }
            await Promise.all(promises)
        }

        console.log(`Generated images for ${companies.length} companies in sitemap: ${siteMap}`)
    }

    /*
     * Generate images for all companies on Partnerbase
     */
    async generateImages() {
        console.log('Generating images...')
        for (const siteMap of this.companiesSiteMaps) {
            await this._generateImagesFromSiteMap(siteMap)
        }
        console.log('Image generation complete.')
    }

    /*
     * Cleanup the image generator
     */
    cleanup() {
        this.snap.stop()
        console.log('Cleanup complete.')
    }
}

module.exports = ImageGenerator
