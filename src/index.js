const ImageGenerator = require('./image-generator')

const baseUrl = process.env.SERVER_API_BASE_URL || 'https://www.partnerbase-dev.com/'

console.log('Starting image generation using URL:', baseUrl)

const imageGenerator = new ImageGenerator(baseUrl)
imageGenerator
    .setup()
    .then(async () => {
        await imageGenerator.generateImages()
    }).then(() => {
        imageGenerator.cleanup()
        console.log('All done!')
    })
